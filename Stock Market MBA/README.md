# Stock Market MBA #

makeMBASec.py script would automatically download global securities and their related details (e.g. sector, ipo dates etc.) from Stock Market MBA website.

The 2 raw Excel files, i.e. Global Stock Screener_MBA.xlsx and U.S. Stock Screener_MBA.xlsx, only contains the issuer and ticker symbol information. The script would use the ticker information to extract other related information from Stock Market MBA website and generate the final file, i.e. MBA_GLOBAL_SECURITIES.csv.