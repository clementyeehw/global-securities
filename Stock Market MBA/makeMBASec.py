# Import Python libraries
from bs4 import BeautifulSoup, NavigableString
from concurrent.futures import ThreadPoolExecutor
import datetime as dt
import gc
import json
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys
import time


# Initialise global variables
download_path = os.environ['USERPROFILE'] + \
                r'\Dropbox (SciBetaTeam)\SciBetaTeam\Research\Global Securities\Stock Market MBA'
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)' + \
           'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
        
    
# Initialise a Firefox Session 
def start_webpage(log_path, download_path=None, mime=None):
    ''' 
    Initialise a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Create profile preferences #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    profile.native_events_enabled = True
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')

    # Create option preferences #                
    options = Options()
    options.add_argument('--headless')
    
    # Initialise a Firefox browser session #
    browser = webdriver.Firefox(firefox_profile=profile, 
                                options=options, service_log_path=log_path)
    
    return browser


# Navigate Stock Market MBA Website and get basic details of all securities
def navigate_webpage(download_path):
    '''
    Navigate Stock Market MBA Website and get all securities.
    Parameter:
        download_path : String containing the local download filepath.
                        Default value is None
    Return:
        stocks        : DataFrame containing stock issuer and ticker symbol
    '''
    # Initialise numpy array #
    stocks = np.empty((0,3), str)
    
    # Initialise a Firefox browser #      
    browser = start_webpage(log_path = download_path + '\\geckodriver.log',
                            download_path = download_path,
                            mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    
    # Send GET request to weblink # 
    browser.get('https://stockmarketmba.com/globalstockscreener.php')
    
    # Wait for browser to complete loading and query all stocks #
    button = WebDriverWait(browser, 20)\
                  .until(EC.element_to_be_clickable((By.NAME, 'hideform')))
    button.click()
    button = WebDriverWait(browser, 20)\
                  .until(EC.element_to_be_clickable((By.LINK_TEXT, 'View all')))
    button.click()
    time.sleep(15)
    
    # Send GET request to weblink # 
    browser.get('https://stockmarketmba.com/stockscreener.php')

    # Wait for browser to complete loading and query all stocks #
    button = WebDriverWait(browser, 20)\
                  .until(EC.element_to_be_clickable((By.NAME, 'assetclass')))
    button.send_keys('Common stocks, REITs, MLPs and ADRs')
    button.send_keys(Keys.ENTER)
    button = WebDriverWait(browser, 20)\
                  .until(EC.element_to_be_clickable((By.LINK_TEXT, 'View all')))
    button.click()
    
    # Download basic details of all US securities #
    button = WebDriverWait(browser, 60)\
                  .until(EC.element_to_be_clickable((By.LINK_TEXT, 'Excel')))
    button.click()
    
    # Merge all securities #    
    non_us_stocks = pd.read_excel(download_path + r'\Global Stock Screener.xlsx', 
                                  usecols=['Symbol', 'Description'],
                                  skiprows=1)
    us_stocks = pd.read_excel(download_path + r'\U.S. Stock Screener.xlsx', 
                              usecols=['Symbol', 'Description'],
                              skiprows=1)
    stocks = non_us_stocks.append(us_stocks)
    
    # Close browser #
    browser.close() 
    
    return stocks


# Navigate Stock Market MBA Website and get advanced details of all securities   
def scrape_stocks():
    ''' 
    Retrieve the advanced details for all stocks.
    Return:
        sec_data : DataFrame containing stocks and their relevant details
    '''
    # Initialise parameters #
    ## Tickers ##
    stocks = navigate_webpage(download_path)
    tickers = list(stocks['Symbol'])
    ## Empty list ##
    stock_data = []
    
    # Loop through each target weblink and get security details # 
    with ThreadPoolExecutor() as executor:
        results = executor.map(get_advanced_info, tickers)
        for result in results:
            if len(result) != 0:
                stock_data.append(result)
    
    # Convert into DataFrame and preprocess the data #
    sec_data = pd.DataFrame(stock_data)
    sec_data = pd.merge(stocks, sec_data, how='inner', on='Symbol')
    sec_data = preprocess_mba_data(sec_data)
    
    # Download processed DataFrame #
    sec_data.to_csv(download_path + '\\MBA_GLOBAL_SECURITIES.csv', 
                    index=False)
    
    return sec_data


def get_advanced_info(ticker):
    '''
    Get basic details of all securities. 
    Parameter:
        ticker       : Security ticker   
    Return:
        sec_data     : Dictionary containing advanced details of target security
    '''
    # Initialise parameters #
    weblink = 'https://stockmarketmba.com/analyze.php?s=' + ticker
    sec_data = {} 
    
    try:
        # Send GET request #
        response = requests.get(weblink, headers=headers)
    except Exception:
        # Print status message #
        print('Failed to access weblink: %s' % weblink)
    else:
        # Apply Beautiful Soup to scrape details #
        soup = BeautifulSoup(response.text, 'html.parser')
        
        if soup.find('div', {'class':'col-sm-5'}) and soup.find('div', {'class':'col-sm-7'}):
            ## Initialise list ##
            values = ['Global Equity']
            ## Get dictionary keys ##
            keys = soup.find('div', {'class':'col-sm-5'}).get_text(strip=True).split(':')
            keys.remove('')
            ## Get dictionary values ##
            lines = soup.find('div', {'class':'col-sm-7'}).find_all('br')
            lines = [line.nextSibling 
                     if line.nextSibling != '\n' or line.nextSibling != None
                     else line.nextSibling.nextSibling
                     for line in lines]
            for line in lines:
                if not isinstance(line, NavigableString):
                    if (line['href'] == '#' or re.search('exchange.php', line['href'])):
                        values.append(line.text.strip())
                    else:
                        values.append(line['href'])
                else:
                    values.append(line.strip())
            ## Fill dictionary ##
            for idx, key in enumerate(keys):
                sec_data[key] = values[idx]
            sec_data['Symbol'] = ticker
            try:
                sec_data['Corporate website'] = soup.find('div', {'class':'col-md-8'})\
                                                    .find('a')['href']
            except Exception:
                sec_data['Corporate website'] = '' 
            
        # Free up memory #
        clean_responses(response, soup)
    
    return sec_data
    

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def clean_responses(response, soup=None):
    '''
    Clear all the the responses to free up memory. 
    Parameters:
        response      : Response from GET requests
        soup          : Parsed BeautifulSoup     
    '''
    # Close response #
    if response is not None:
        response.close()
        response = None
    
    # Decompose soup if soup is valid #
    if soup is not None:
        soup.decompose()
        
    # Clear all the waste #
    gc.collect()
    

# Enrich and preprocess Stock Market MBA data with BBG API data  
def preprocess_mba_data(sec_data):
    '''
    Enrich and preprocess Stock Market MBA data with BBG API data.
    Parameter:
        sec_data     : DataFrame containing stocks and their raw details
    Return:
        rev_data       : DataFrame containing stocks and their pre-processed details
    '''
    # Initialise parameter #
    bbg_data = np.empty((0,6), str)
    
    # Massage datetime string into datetime format and drop irrelevant dates #
    sec_data['IPO date'] = \
        [date.date() for date in pd.to_datetime(sec_data['IPO date'], format='%m/%d/%Y')]
    sec_data = sec_data.loc[sec_data['IPO_DATE'] != dt.datetime.today().date()]
    
    # Split exchange code and ticker #
    sec_data['Stock exchange'] = sec_data['Symbol'].str.split(':').str[0]
    sec_data.loc[~sec_data['Symbol'].str.contains(':'), ['Stock exchange']] = 'US'
    sec_data['Symbol'] = sec_data['Symbol'].str.split(':').str[-1]
    
    # Query ids and retrieve BBG API data #
    ## Tickers and exchange codes ##
    ids = sec_data[['Symbol', 'Stock exchange']].values.tolist()
    chunked_ids = chunks(ids, 100)
    for chunked_id in chunked_ids:
        data = get_bbg_data(chunked_id, 'TICKER')
        bbg_data = np.vstack((bbg_data, data))
    ## ISINs and exchange codes ##
    ids = sec_data.loc[sec_data['ISIN'].notnull(), ['ISIN', 'Stock exchange']]\
                  .values.tolist()
    chunked_ids = chunks(ids, 100)
    for chunked_id in chunked_ids:
        data = get_bbg_data(chunked_id, 'ID_ISIN')
        bbg_data = np.vstack((bbg_data, data))
    
    # Convert bbg_data to DataFrame and drop all duplicates and null values #
    bbg_data = pd.DataFrame(bbg_data, columns=['Sym', 'Stock exchange',
                                               'BBG_TICKER', 'ID', 
                                               'BBG_SECURITY', 'BBG_SECURITY_TYPE'])
    bbg_data = bbg_data.replace('', np.nan)
    bbg_data.dropna(subset=['ID'], inplace=True)
    bbg_data.drop_duplicates(inplace=True)
    bbg_data.index = range(len(bbg_data))
    
    # Merge bbg_data with stock data #
    bbg_data = pd.merge(bbg_data, sec_data[['ISIN', 'Symbol', 'Stock exchange']], 
                        how='left', 
                        left_on=['Sym', 'Stock exchange'],
                        right_on=['ISIN', 'Stock exchange'])
    bbg_data.loc[bbg_data['ISIN'].notnull(), ['Sym']] = \
        bbg_data.loc[bbg_data['ISIN'].notnull(), 'Symbol']
    del bbg_data['ISIN'], bbg_data['Symbol']
    bbg_data.columns = ['Symbol', 'Stock exchange', 'BBG_TICKER', 'ID', 
                        'BBG_SECURITY', 'BBG_SECURITY_TYPE']
    
    sec_data = pd.merge(sec_data, bbg_data, how='left', on=['Symbol', 'Stock exchange'])
    rev_data = sec_data.dropna(subset=['ID'])
    rev_data.drop_duplicates(subset=['Symbol', 'BBG_TICKER', 'ID'], inplace=True)
    
    # Rearrange columns and sort data #
    rev_data = rev_data[['Symbol', 'ID', 'Description', 'Corporate website',
                         'GICS sector', 'Industry', 'Category2', 'Country of incorporation', 
                         'Stock exchange', 'Exchange country', 
                         'BBG_TICKER', 'BBG_SECURITY', 'BBG_SECURITY_TYPE',
                         'ISIN', 'CUSIP', 'Sedol', 'Outstanding shares', 'IPO date',
                         'All SEC filings', 'Quarterly filings', 'Annual filings']]
    rev_data.columns = ['MBA_ID', 'BBG_ID', 'COMPANY_NAME', 'COMPANY_WEBSITE',
                        'GICS_SECTOR', 'INDUSTRY', 'SECURITY_TYPE', 'COUNTRY_INCORP',
                        'BBG_EXCH_CODE_SHORT', 'MBA_EXCH_CODE_LONG', 
                        'BBG_TICKER', 'BBG_SECURITY', 'BBG_SECURITY_TYPE', 
                        'ISIN', 'CUSIP', 'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE',
                        'SEC_ALL_FILING', 'SEC_QTR_FILING', 'SEC_ANNUAL_FILING']
    rev_data['MBA_ID'] = rev_data['BBG_ID'] + '-' + rev_data['MBA_ID']
    
    return rev_data
    
    
def get_bbg_data(ids, idType):
    ''' 
    Return bbg data for each ID fed to the openfigi API.
    Parameter:
        ids        : List containing target ids and stock exchange
        idType     : Type of ID being queried
    Return:
        bbg_data   : Numpy array containing the security types of the target tickers
    ''' 
    # Initialise parameters #
    ## API ##
    openfigi_url = 'https://api.openfigi.com/v2/mapping'
    ## Headers ##
    headers = {'Content-Type':'text/json', 
               'X-OPENFIGI-APIKEY':'e6982c05-abe9-4d39-a910-b8982873bfae'}
    ## Payload ##
    payload = json.dumps([{"idType": idType, 
                           "idValue": id_, 
                           "exchCode": exchCode} 
                            for id_, exchCode in ids])
    ## bbg_data ##
    bbg_data = np.empty((0,6), str)
    
    # Send request for each ID and stock exchange #
    response = requests.post(openfigi_url, data=payload, headers=headers)
    try: 
        response.json()
    except Exception:
        pass
    else:         
        data = np.array([[ids[idx][0], ids[idx][1],
                          res['data'][0]['ticker'],
                          res['data'][0]['uniqueID'],
                          res['data'][0]['securityType'],
                          res['data'][0]['securityType2']]
                         if 'data' in list(res.keys())
                         else [ids[idx][0], ids[idx][1], '', '', '', '']
                         for idx, res in enumerate(response.json())])
        bbg_data = np.vstack((bbg_data, data))
         
    return bbg_data
    
if __name__ == 'scrape_stocks':
    scrape_stocks(sys.argv)