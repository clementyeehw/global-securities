# Import Python libraries
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
import gc
import json
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import sys
import time


# Initialise global variables
download_path = os.environ['USERPROFILE'] + \
                r'\Dropbox (SciBetaTeam)\SciBetaTeam\Research\Global Securities\Woodseer'
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)' + \
           'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
    

# Initialise a Firefox Session 
def start_webpage(log_path, download_path=None, mime=None):
    ''' 
    Initialise a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Create profile preferences #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    profile.native_events_enabled = True
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')

    # Create option preferences #                
    options = Options()
    options.add_argument('--headless')
    
    # Initialise a Firefox browser session #
    browser = webdriver.Firefox(firefox_profile=profile, 
                                options=options, service_log_path=log_path)
    
    return browser


# Navigate Woodseer website and get basic details of all securities
def get_exchange_info(download_path):
    '''
    Get basic details of all exchanges featured on Woodseer. 
    Parameter:
        download_path : String containing the local download filepath.
                        Default value is None
    Return:
        exchanges    : List containing basic details of all exchanges
    '''
    # Initialise parameter #
    exchanges = []
    
    # Initialise a Firefox browser #      
    browser = start_webpage(log_path = download_path + '\\geckodriver.log')
    
    # Send GET request to weblink # 
    browser.get('https://www.woodseer.global/stock_exchanges')
    
    # Scrape web for basic details of exchanges #
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    regions = soup.find_all('div', {'class': 'table-wrap divider'})
    for region in regions:
        countries = region.find_all('tr')
        for country in countries:
            exchanges.append([
                country.find('a').text.strip(),
                'https://www.woodseer.global' + country.find('a')['href']
                ])
    
    # Close browser #
    browser.close()
    
    return exchanges


def get_stocks():
    ''' 
    Retrieve the basic details for all securities.
    Return:
        sec_data : DataFrame containing stocks and their relevant details
    '''
    # Initialise parameters #
    ## Number of instances ##
    max_instances = 10
    ## Weblinks ##
    exchanges = get_exchange_info(download_path)
    weblinks = [weblink for exchange, weblink in exchanges]
    chunked_weblinks = chunks(weblinks, max_instances)
    ## Browsers ##
    browsers = [start_webpage(log_path = download_path + '\\geckodriver.log')
                for i in range(max_instances)]
    ## Empty array #
    sec_data = np.empty((0,9), str)
    
    # Loop through each target weblink and get security details # 
    with ThreadPoolExecutor(max_workers=max_instances) as executor:
        for chunked_weblink in chunked_weblinks:
            results = executor.map(get_stock_info, browsers, chunked_weblink)
            for result in results:
                if len(result) != 0:
                    sec_data = np.vstack((sec_data, result))
    
    # Close browsers #
    [browser.close() for browser in browsers]    
    
    # Convert data into DataFrame and preprocess the data #
    sec_data = pd.DataFrame(sec_data,
                            columns=['COMPANY_NAME', 'SECURITY', 'TYPE',
                                     'STATUS', 'TICKER', 'ISIN',
                                     'EXCHANGE_LONG', 'EXCHANGE_SHORT', 'WS_WEBLINK'])
    sec_data = preprocess_ws_data(sec_data)
    
    # Download processed DataFrame #
    sec_data.to_csv(download_path + '\\WS_GLOBAL_SECURITIES.csv', 
                    index=False)
    
    return sec_data


def get_stock_info(browser, weblink):
    '''
    Get basic details of all securities. 
    Parameters:
        browser      : Instantiated Firefox browser
        weblink      : Woodseer exchange weblink  
    Return:
        sec_data     : Array containing basic details of exchange securities
    '''
    # Initialise parameter #
    sec_data = np.empty((0,9), str)
    
    try:
        # Send GET request #
        browser.get(weblink)
    except Exception:
        # Print status message #
        print('Failed to access weblink: %s' % weblink)
    else:
        # Apply Beautiful Soup to scrape details #
        soup = BeautifulSoup(browser.page_source, 'html.parser')
        
        # Get last page #
        if soup.find('div', {'class':'pagination'}):
            last_page = int(soup.find('div', {'class':'pagination'})
                                .find_all('a', {'aria-label':re.compile(r'Page \d')})[-1]
                                .text.strip())            
        else:
            last_page = 1

        # Get exchange name #
        temp_name = weblink.split('/')[-1].split('-')
        exc_long = ' '.join(temp_name[:-1]).title()
        exc_short = temp_name[-1].upper()
            
        # Loop through each page #
        for page in range(1, last_page + 1):
            weblink_ = weblink + '?page=%s' % str(page)
            if page > 1:
                ## Send GET request ##
                browser.get(weblink_)
                time.sleep(1)
                ## Apply Beautiful Soup to scrape details ##
                soup = BeautifulSoup(browser.page_source, 'html.parser')
            ## Scrape all securities ##
            if not soup.find('td', {'class':'empty'}):
                rows = soup.find('div', {'class':'table-wrap'}).find_all('tr')[1:]
                for row in rows:
                    data = row.find_all('td')
                    host_link = ['https://www.woodseer.global' + dat.find('a')['href']
                                 for dat in data 
                                 if dat.find('a')][0]
                    temp_data = [dat.text.strip() for dat in data] + \
                                [exc_long, exc_short, host_link]
                    sec_data = np.vstack((sec_data, np.array([temp_data])))
            ## Free up memory ##
            clean_responses(None, soup)
    
    return sec_data
    

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def clean_responses(response, soup=None):
    '''
    Clear all the the responses to free up memory. 
    Parameters:
        response      : Response from GET requests
        soup          : Parsed BeautifulSoup     
    '''
    # Close response #
    if response is not None:
        response.close()
        response = None
    
    # Decompose soup if soup is valid #
    if soup is not None:
        soup.decompose()
        
    # Clear all the waste #
    gc.collect()
    
    
# Enrich and preprocess Woodseer data with BBG API data  
def preprocess_ws_data(sec_data):
    '''
    Enrich and preprocess Woodseer data with BBG API data.
    Parameter:
        sec_data     : DataFrame containing stocks and their raw details
    Return:
        rev_data     : DataFrame containing stocks and their pre-processed details
    '''
    # Initialise parameter #
    bbg_data = np.empty((0,7), str)
    
    # Replace None as np.nan #
    sec_data = sec_data.replace('none', np.nan)
    
    # Remove errant securities #
    sec_data = sec_data.loc[~sec_data['COMPANY'].isin(['Vodafone K.K.', 'SCISYS Group Plc'])]
    
    # Query ids and retrieve BBG API data #
    ## ISINs and exchange codes ##
    ids = sec_data.loc[sec_data['ISIN'].notnull(), ['ISIN', 'EXCHANGE_SHORT']]\
                  .values.tolist()
    chunked_ids = chunks(ids, 100)
    for chunked_id in chunked_ids:
        data = get_bbg_data(chunked_id, 'ID_ISIN')
        bbg_data = np.vstack((bbg_data, data))
    ## Tickers and exchange codes ##
    ids = sec_data.loc[sec_data['ISIN'].isnull(), ['TICKER', 'EXCHANGE_SHORT']]\
                  .values.tolist()
    chunked_ids = chunks(ids, 100)
    for chunked_id in chunked_ids:
        data = get_bbg_data(chunked_id, 'TICKER')
        bbg_data = np.vstack((bbg_data, data))

    # Convert bbg_data to DataFrame and drop all duplicates and null values #
    bbg_data = pd.DataFrame(bbg_data, columns=['Symbol', 'EXCHANGE_SHORT',
                                               'BBG_TICKER', 'BBG_EXCH_CODE', 'ID',
                                               'BBG_SECURITY', 'BBG_SECURITY_TYPE'])
    bbg_data = bbg_data.replace('', np.nan)
    bbg_data.dropna(subset=['ID'], inplace=True)
    bbg_data.drop_duplicates(inplace=True)
    bbg_data.index = range(len(bbg_data))
    
    # Merge bbg_data with stock data #
    sec_data['Symbol'] = np.nan
    sec_data.loc[sec_data['ISIN'].notnull(), ['Symbol']] = \
        sec_data.loc[sec_data['ISIN'].notnull(), 'ISIN']
    sec_data.loc[sec_data['ISIN'].isnull(), ['Symbol']] = \
        sec_data.loc[sec_data['ISIN'].isnull(), 'TICKER']
    rev_data = pd.merge(sec_data, bbg_data, how='left', on=['Symbol', 'EXCHANGE_SHORT'])
    
    # Rearrange columns and sort data #
    rev_data.dropna(subset=['ID'], inplace=True)
    rev_data.drop_duplicates(subset=['ISIN', 'ID'], inplace=True)
    rev_data.drop_duplicates(subset=['TICKER', 'ID'], inplace=True)
    rev_data['WS_ID'] = rev_data.groupby('ID').cumcount() + 1
    rev_data['WS_ID'] = rev_data['BBG_ID'] + '-' + rev_data['WS_ID'].astype(str)
    rev_data = rev_data[['WS_ID', 'ID', 'COMPANY', 'SECURITY', 'TYPE', 'STATUS', 
                         'BBG_EXCH_CODE', 'EXCHANGE_LONG', 'EXCHANGE_SHORT', 
                         'TICKER', 'BBG_TICKER', 'BBG_SECURITY', 'BBG_SECURITY_TYPE',
                         'ISIN', 'WEBLINK']]
    rev_data.columns = ['WS_ID', 'BBG_ID', 'COMPANY_NAME', 'SECURITY', 'TYPE', 'STATUS', 
                        'BBG_EXCH_CODE_SHORT', 'WS_EXCH_CODE_LONG', 'MIC', 
                        'WS_TICKER', 'BBG_TICKER', 'BBG_SECURITY', 'BBG_SECURITY_TYPE',
                        'ISIN', 'WS_WEBLINK']

    return rev_data
    

def get_bbg_data(ids, idType):
    ''' 
    Return bbg data for each ID fed to the openfigi API.
    Parameter:
        ids        : List containing target ids and stock exchange
        idType     : Type of ID being queried
    Return:
        bbg_data   : Numpy array containing the security types of the target tickers
    ''' 
    # Initialise parameters #
    ## API ##
    openfigi_url = 'https://api.openfigi.com/v2/mapping'
    ## Headers ##
    headers = {'Content-Type':'text/json', 
               'X-OPENFIGI-APIKEY':'e6982c05-abe9-4d39-a910-b8982873bfae'}
    ## Payload ##
    payload = json.dumps([{"idType": idType, 
                           "idValue": id_.replace(' ','').strip(), 
                           "micCode": micCode} 
                            for id_, micCode in ids])
    ## bbg_data ##
    bbg_data = np.empty((0,7), str)
    
    # Send request for each ID and stock exchange #
    response = requests.post(openfigi_url, data=payload, headers=headers)
    try: 
        response.json()
    except Exception:
        pass
    else:         
        data = np.array([[ids[idx][0], ids[idx][1],
                          res['data'][0]['ticker'],
                          res['data'][0]['exchCode'],
                          res['data'][0]['uniqueID'],
                          res['data'][0]['securityType'],
                          res['data'][0]['securityType2']]
                         if 'data' in list(res.keys())
                         else [ids[idx][0], ids[idx][1], '', '', '', '', '']
                         for idx, res in enumerate(response.json())])
        bbg_data = np.vstack((bbg_data, data))
         
    return bbg_data

if __name__ == 'get_stocks':
    get_stocks(sys.argv)