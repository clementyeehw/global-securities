# Import Python Libraries
import importlib.util
import os
import numpy as np
import pandas as pd
import re


# Initialise global variables
## Please clone Bitbucket repository into local ESG directory ##
script_path = os.environ['USERPROFILE'] + r'\ESG\global-securities' 
download_path = os.environ['USERPROFILE'] + \
                r'\Dropbox (SciBetaTeam)\SciBetaTeam\Research\Global Securities'

# Run the Python scripts 
def run_script(script_path):
    '''
    Run the designated Python scripts to get stock details.
    Parameter:
        script_path : String containing the path of the python script
    Return:
        sec_data    : DataFrame containing stocks and their relevant details
    '''
    # Import python script as a module #
    spec = importlib.util.spec_from_file_location('get', script_path)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    
    # Output file #
    if re.search('MBA', script_path):
        sec_data = mod.scrape_stocks()
    elif re.search('Woodseer', script_path):
        sec_data = mod.get_stocks()
    else:
        raise('Unacceptable script path. Please check!')
    
    return sec_data


# Merge output files from each Python script
def merge_sources(download_path, script_path, run_scripts=False):
    '''
    Merge output files from each Python script.
    Parameters:
        download_path : String containing the local download filepath
        script_path   : String containing the path of the Python script
        run_script    : Boolean flag to run both Python scripts.
                        Default is False
    Return:
        stocks        : DataFrame containing merged stocks and their relevant details
    '''
    # Verify run_scripts flag #
    if run_scripts:
        ## Run Stock Market MBA script ##
        mba_data = run_script(script_path + r'\Stock Market MBA\makeMBASec.py') 
        ## Run Woodseer script ##
        ws_data = run_script(script_path + r'\Woodseer\makeWSSec.py')
    else:
        ## Import input files from Dropbox ##
        mba_data = pd.read_csv(download_path + r'\Stock Market MBA\MBA_GLOBAL_SECURITIES.csv') 
        ws_data = pd.read_csv(download_path + r'\Woodseer\WS_GLOBAL_SECURITIES.csv')
        
    # Merge both mba_data and ws_data #
    stocks = pd.merge(ws_data, mba_data[['BBG_ID', 'COMPANY_WEBSITE', 'GICS_SECTOR',
                                         'INDUSTRY', 'COUNTRY_INCORP', 'CUSIP',
                                         'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE']],
                      how='left', on='BBG_ID')
    
    # Drop all NaN SEDOLs and duplicates #
    stocks.sort_values(by=['WS_ID'], inplace=True)
    add_rows = stocks.loc[stocks.duplicated(subset='WS_ID', keep=False)]\
                     .dropna(subset=['SEDOL'])
    stocks = stocks.append(add_rows, ignore_index=True)
    add_rows = stocks.loc[stocks.duplicated(subset='WS_ID', keep=False)]\
                     .sort_values(by=['CUSIP', 'SEDOL'])\
                     .drop_duplicates(subset=['BBG_ID'])
    stocks.drop_duplicates(subset=['BBG_ID'], keep=False, inplace=True)
    stocks = stocks.append(add_rows, ignore_index=True)
    
    # Rearrange and rename columns #
    stocks = stocks[['WS_ID', 'BBG_ID', 'COMPANY_NAME', 'COMPANY_WEBSITE', 'GICS_SECTOR', 
                     'INDUSTRY', 'COUNTRY_INCORP', 'BBG_SECURITY', 'BBG_SECURITY_TYPE', 
                     'BBG_EXCH_CODE_SHORT', 'WS_EXCH_CODE_LONG', 'MIC', 'BBG_TICKER', 
                     'ISIN', 'CUSIP', 'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE', 'STATUS']]
    stocks.columns = ['MAIN_ID', 'BBG_ID', 'COMPANY_NAME', 'COMPANY_WEBSITE', 'GICS_SECTOR', 
                      'INDUSTRY', 'COUNTRY_INCORP', 'BBG_SECURITY', 'BBG_SECURITY_TYPE', 
                      'BBG_EXCH_CODE_SHORT', 'EXCH_CODE_LONG', 'MIC', 'BBG_TICKER',  
                      'ISIN', 'CUSIP', 'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE', 'STATUS']
    columns = list(stocks.columns)
    
    # Stack mba_data with stocks #
    stacked = mba_data[['MBA_ID', 'BBG_ID', 'COMPANY_NAME', 'COMPANY_WEBSITE', 'GICS_SECTOR', 
                        'INDUSTRY', 'COUNTRY_INCORP', 'BBG_SECURITY', 'BBG_SECURITY_TYPE', 
                        'BBG_EXCH_CODE_SHORT', 'MBA_EXCH_CODE_LONG', 'BBG_TICKER', 'ISIN', 
                        'CUSIP', 'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE']]
    stacked['STATUS'] = np.nan
    stacked['MIC'] = np.nan
    stacked = stacked[['MBA_ID', 'BBG_ID', 'COMPANY_NAME', 'COMPANY_WEBSITE', 'GICS_SECTOR', 
                       'INDUSTRY', 'COUNTRY_INCORP', 'BBG_SECURITY', 'BBG_SECURITY_TYPE', 
                       'BBG_EXCH_CODE_SHORT', 'MBA_EXCH_CODE_LONG', 'MIC', 'BBG_TICKER', 
                       'ISIN', 'CUSIP', 'SEDOL', 'OUTSTANDING_SHARES', 'IPO_DATE', 'STATUS']]
    stacked.columns = columns
    stocks = stocks.append(stacked, ignore_index=True)
    stocks.drop_duplicates(subset=['BBG_ID', 'ISIN'], inplace=True)
    stocks.dropna(subset=['ISIN'], inplace=True)
    
    # Download processed DataFrame #
    stocks.to_csv(download_path + '\\GLOBAL_SECURITIES.csv', 
                  index=False) 
    
    return stocks

stocks = merge_sources(download_path, script_path)